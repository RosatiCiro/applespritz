//: A UIKit based Playground for presenting user interface
  
import UIKit
import PlaygroundSupport
import AVFoundation
import QuartzCore

class MusicPlayer {
    static let shared = MusicPlayer()
    var audioPlayer: AVAudioPlayer?

    func startBackgroundMusic() {
        if let bundle = Bundle.main.path(forResource: "musica", ofType: "mp3") {
            let backgroundMusic = NSURL(fileURLWithPath: bundle)
            do {
                audioPlayer = try AVAudioPlayer(contentsOf:backgroundMusic as URL)
                guard let audioPlayer = audioPlayer else { return }
                audioPlayer.numberOfLoops = -1
                audioPlayer.prepareToPlay()
                audioPlayer.play()
            } catch {
                print(error)
            }
        }
    }
    
    
    func stopBackgroundMusic() {
        guard let audioPlayer = audioPlayer else { return }
        audioPlayer.stop()
    }
}

class MyViewController: UIViewController {
    
    var manView: UIImageView!
    var earthView: UIImageView!
    var sadView: UIImageView!
    var omiView: UIImageView!
    var omi2View: UIImageView!
    var omi3View: UIImageView!
    var traView: UIImageView!
    var tra2View: UIImageView!
    var tra3View: UIImageView!
    var finalView: UIImageView!
    var textPresentation: UILabel!
    var Label1: UILabel!
    var Label2: UILabel!
    var titleLabel: UILabel!
    var textLabel6 : UILabel!
    var backView: UIImageView!
    var tap = 0

    
    override func loadView() {
        let view = UIView()
        view.backgroundColor = .white
        
       
        //TITOLO
        
        titleLabel = UILabel()
        titleLabel.text = "FOR A BETTER PLACE"
        titleLabel.font = UIFont (name: "Futura-Bold", size: 25)
        titleLabel.textColor = .systemGreen
        titleLabel.frame = CGRect(x: 30, y: 150, width: 400, height: 300)
        view.addSubview(titleLabel)
        
//        Presentazione
        textPresentation = UILabel ()
        
        textPresentation.font = UIFont (name: "Futura", size: 20)
        textPresentation.textColor = .systemGreen
        textPresentation.frame = CGRect(x: 50, y: 350, width: 250, height: 120)
        view.addSubview(textPresentation)
        textPresentation.numberOfLines = 10
        textPresentation.isHidden = true
        self.view = view
        
        
        
//        Scritta 1
        
       Label1 = UILabel ()
       
        Label1.font = UIFont (name: "Futura", size: 20)
        Label1.textColor = .systemGreen
        Label1.frame = CGRect(x: 16, y: 350, width: 250, height: 200)
        view.addSubview(Label1)
        Label1.numberOfLines = 10
        Label1.isHidden = true
        self.view = view
        
        //  Scritta 2
        
       Label2 = UILabel ()
        
        Label2.font = UIFont (name: "Futura", size: 20)
        Label2.textColor = .systemGreen
        Label2.frame = CGRect(x: 16, y: 350, width: 250, height: 200)
        Label2.numberOfLines = 10
        
        
       
        view.addSubview(Label2)
        Label2.isHidden = true
        self.view = view
                
        
        // To be continued
        
        textLabel6 = UILabel ()
        textLabel6.text = "TO BE CONTINUED..."
        textLabel6.font = UIFont (name: "Futura", size: 25)
        textLabel6.textColor = .systemGreen
        textLabel6.frame = CGRect(x: 100 , y: 400, width: 400, height: 300)
        view.addSubview(textLabel6)
        textLabel6.isHidden = true
        self.view = view
        
//        Immagine intro
        
        earthView = UIImageView()
        earthView.image = UIImage(named:"earth.png")
        earthView.frame = CGRect(x: 100, y: 350, width: 350, height: 350)
        view.addSubview(earthView)
        
    UIView.animate(withDuration: 4) {
    self.earthView.transform = self.earthView.transform.rotated(by: .pi/1)}

        
        
        //Sfondo
        
        backView = UIImageView()
        backView.image = UIImage(named: "landscape.png")
        backView.frame = CGRect(x: 0, y: 20, width: 370, height: 290)
        view.addSubview(backView)
        backView.isHidden = true
        
        //Gennaro felice
        
        manView = UIImageView()
        manView.image = UIImage(named: "gennaro.png")
        manView.frame = CGRect(x: 1, y: 350, width: 80, height: 190)
        manView.isHidden = true
        view.addSubview(manView)
        
//        Gennaro triste
        
        sadView = UIImageView()
        sadView.image = UIImage(named: "Gennaro triste.png")
        sadView.frame = CGRect(x: 250, y: 350, width: 80, height: 190)
        view.addSubview(sadView)
        sadView.isHidden = true
        
        
//        Omino cattivo
        
        omiView = UIImageView()
        omiView.image = UIImage(named: "Amica2.png")
        omiView.frame = CGRect(x: 1, y: 320, width: 50,height: 100)
        view.addSubview(omiView)
        omiView.isHidden = true
        
        
//        Omino cattivo 2
        
        omi2View = UIImageView()
        omi2View.image = UIImage(named: "Amico1.png")
        omi2View.frame = CGRect(x: 1, y: 400, width: 50,height: 100)
        view.addSubview(omi2View)
        omi2View.isHidden = true
        
//        Omino cattivo 3
        
        omi3View = UIImageView()
        omi3View.image = UIImage(named: "Amico3.png")
        omi3View.frame = CGRect(x: 1, y: 500, width: 50,height: 100)
        view.addSubview(omi3View)
        omi3View.isHidden = true
        
//      Cartaccia
        
        traView = UIImageView()
        traView.image = UIImage(named: "trash.png")
        traView.frame = CGRect(x:1, y:390, width: 25, height: 25)
        view.addSubview(traView)
        traView.isHidden = true
        
//      Cartaccia2
        
        tra2View = UIImageView()
        tra2View.image = UIImage(named: "trash.png")
        tra2View.frame = CGRect(x:1, y:510, width: 25, height: 25)
        view.addSubview(tra2View)
        tra2View.isHidden = true
        
//      Cartaccia3
        
        tra3View = UIImageView()
        tra3View.image = UIImage(named: "trash.png")
        tra3View.frame = CGRect(x:1, y:590, width: 25, height: 25)
        view.addSubview(tra3View)
        tra3View.isHidden = true


//        Fine
        finalView = UIImageView ()
        finalView.image = UIImage (named: "to be.png")
        finalView.frame = CGRect(x:45, y:250, width: 280, height: 200)
        view.addSubview(finalView)
        finalView.isHidden = true
        
        //Scorrimento e Traslazione

    
        
        //gestione del tap
        
        let gestureTapRecognizer = UITapGestureRecognizer ( target: self, action: #selector(buttonTapped) )
        self.view.addGestureRecognizer(gestureTapRecognizer)
        
    }
    @objc func buttonTapped (){
        
        tap += 1
        print ("tapped")
        
        
        if (tap == 1){
        MusicPlayer.shared.startBackgroundMusic()
        manView.isHidden = false
        UIView.animate(withDuration: 2) {
        self.manView.transform =
            CGAffineTransform(translationX: 249, y: 0)
    
            }
            backView.isHidden = false
            titleLabel.isHidden = true
            earthView.isHidden = true
        }
         
        else if (tap == 2) {
            textPresentation.isHidden = false
            textPresentation.setTextWithTypeAnimation(typedText: "Hi , my name is \nGennaro" , characterDelay: 5) //less delay is faster
        
        }
            
        else if (tap == 3){
            textPresentation.isHidden = true
            Label1.isHidden = false
            Label1.setTextWithTypeAnimation(typedText: "        I live in Naples, \n     I really love my city. \n Here you can find delicious \ndishes, wonderful landscape \n     and friendly people. " , characterDelay: 3) //less delay is faster
            }
        
            
        else if (tap == 4){
            Label1.isHidden = true
            Label2.isHidden = false
            Label2.setTextWithTypeAnimation(typedText: "But a lot of people here, \n     don't take care of our \n  beautiful  environment \n     and don't recycle  \n   their trash properly. " , characterDelay: 3) //less delay is faster
            
        }
        else if (tap == 5){
            Label2.isHidden = true
            omiView.isHidden = false
            traView.isHidden = false
        UIView.animate(withDuration: 2) {
            self.omiView.transform =
            CGAffineTransform(translationX: 99, y: 0)
            self.traView.transform =
            CGAffineTransform(translationX: 89, y: 0)
            }
        }
        
        else if  (tap == 6){
            omi2View.isHidden = false
            tra2View.isHidden = false

            UIView.animate(withDuration: 2){
            self.omi2View.transform =
            CGAffineTransform(translationX: 49, y: 0)
            self.tra2View.transform =
            CGAffineTransform(translationX: 39, y: 0)

            }
        }
            
        
        
        else if (tap == 7){
            omi3View.isHidden = false
            tra3View.isHidden = false
            manView.isHidden = true
            sadView.isHidden = false

                        UIView.animate(withDuration: 2){
            self.omi3View.transform =
            CGAffineTransform(translationX: 99, y: 0)
            self.tra3View.transform =
            CGAffineTransform(translationX: 119, y: 0)

            }

        }
        
        
        else if (tap == 8){
            omiView.isHidden = true
            omi2View.isHidden = true
            omi3View.isHidden = true
            traView.isHidden = true
            tra2View.isHidden = true
            tra3View.isHidden = true
            sadView.isHidden = true
            textLabel6.isHidden = false
            titleLabel.isHidden = true
            manView.isHidden = true
            backView.isHidden = true
            finalView.isHidden = false
            
            
            }
        else if (tap == 9) {MusicPlayer.shared.stopBackgroundMusic()}
        }
        
    }
extension UILabel {
    func setTextWithTypeAnimation(typedText: String, characterDelay: TimeInterval = 5.0) {
        text = ""
        var writingTask: DispatchWorkItem?
        writingTask = DispatchWorkItem { [weak weakSelf = self] in
            for character in typedText {
                DispatchQueue.main.async {
                    weakSelf?.text!.append(character)
                }
                Thread.sleep(forTimeInterval: characterDelay/100)
            }
        }

        if let task = writingTask {
            let queue = DispatchQueue(label: "typespeed", qos: DispatchQoS.userInteractive)
            queue.asyncAfter(deadline: .now() + 0.05, execute: task)
        }
    }

}


// Present the view controller in the Live View window
PlaygroundPage.current.liveView = MyViewController()
