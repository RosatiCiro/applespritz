//: A UIKit based Playground for presenting user interface
  
import UIKit
import PlaygroundSupport
import AVFoundation




class MusicPlayer {
    static let shared = MusicPlayer()
    var audioPlayer: AVAudioPlayer?

    func startBackgroundMusic() {
        if let bundle = Bundle.main.path(forResource: "musica2", ofType: "mp3") {
            let backgroundMusic = NSURL(fileURLWithPath: bundle)
            do {
                audioPlayer = try AVAudioPlayer(contentsOf:backgroundMusic as URL)
                guard let audioPlayer = audioPlayer else { return }
                audioPlayer.numberOfLoops = -1
                audioPlayer.prepareToPlay()
                audioPlayer.play()
            } catch {
                print(error)
            }
        }
    }
    
    
    func stopBackgroundMusic() {
        guard let audioPlayer = audioPlayer else { return }
        audioPlayer.stop()
    }
}



class MyViewController: UIViewController {

var RecycleView: UIImageView!
var textLabel1: UILabel!
    var Label4: UILabel!
var tap = 0
    var backView : UIImageView!
    var omiView: UIImageView!
    var omi2View: UIImageView!
    var omi3View: UIImageView!
    var traView: UIImageView!
    var tra2View: UIImageView!
    var tra3View: UIImageView!
    var Label1 : UILabel!

    
    
override func loadView() {
    let view = UIView()
    view.backgroundColor = .white
    
   MusicPlayer.shared.startBackgroundMusic()
    //Sfondo
    
    backView = UIImageView()
    backView.image = UIImage(named: "Landscape.png")
    backView.frame = CGRect(x: 0, y: 20, width: 370, height: 290)
    view.addSubview(backView)
    
    
    // Omino cattivo
            
            omiView = UIImageView()
            omiView.image = UIImage(named: "Amica2.png")
            omiView.frame = CGRect(x: 75, y: 320, width: 50,height: 100)
            view.addSubview(omiView)
           
            
            
    //        Omino cattivo 2
            
            omi2View = UIImageView()
            omi2View.image = UIImage(named: "Amico1.png")
            omi2View.frame = CGRect(x: 20, y: 400, width: 50,height: 100)
            view.addSubview(omi2View)
            
            
    //        Omino cattivo 3
            
            omi3View = UIImageView()
            omi3View.image = UIImage(named: "Amico3.png")
            omi3View.frame = CGRect(x: 100, y: 500, width: 50,height: 100)
            view.addSubview(omi3View)
            
            
    //      Cartaccia
            
            traView = UIImageView()
            traView.image = UIImage(named: "trash.png")
            traView.frame = CGRect(x:120, y:390, width: 25, height: 25)
            view.addSubview(traView)
            
            
    //      Cartaccia2
            
            tra2View = UIImageView()
            tra2View.image = UIImage(named: "trash.png")
            tra2View.frame = CGRect(x:30, y:510, width: 25, height: 25)
            view.addSubview(tra2View)
            
            
    //      Cartaccia3
            
            tra3View = UIImageView()
            tra3View.image = UIImage(named: "trash.png")
            tra3View.frame = CGRect(x:90, y:590, width: 25, height: 25)
            view.addSubview(tra3View)
            
    //        Immagine
   
    RecycleView = UIImageView ()
    RecycleView.image = UIImage (named: "tobe.png")
    RecycleView.frame = CGRect(x:45, y:250, width: 280, height: 200)
    view.addSubview(RecycleView)
    RecycleView.isHidden = true
    
    
//    Testo
    textLabel1 = UILabel ()
    
    textLabel1.font = UIFont (name: "Futura", size: 20)
    textLabel1.textColor = .systemGreen
    textLabel1.frame = CGRect(x: 60, y: 360, width: 250, height: 300)
    view.addSubview(textLabel1)
    textLabel1.numberOfLines = 10
    textLabel1.isHidden = true
    self.view = view
    
// Testo fine
    
   
    Label1 = UILabel ()
          
           Label1.font = UIFont (name: "Futura", size: 20)
           Label1.textColor = .systemGreen
           Label1.frame = CGRect(x: 180, y: 360, width: 180, height: 200)
           view.addSubview(Label1)
           Label1.numberOfLines = 10
           Label1.isHidden = true
          self.view = view
    
    Label4 = UILabel ()
    Label4.text = "TO BE CONTINUED..."
     Label4.font = UIFont (name: "Futura", size: 25)
     Label4.textColor = .systemGreen
     Label4.frame = CGRect(x: 100 , y: 450, width: 500, height: 300)
     view.addSubview(Label4)
     Label4.isHidden = true
     self.view = view
    
    
    //gestione del tap
           
    let gestureTapRecognizer = UITapGestureRecognizer ( target: self, action: #selector(buttonTapped) )
    self.view.addGestureRecognizer(gestureTapRecognizer)
           
       }
    
    @objc func buttonTapped (){
           
    tap += 1
    print ("tapped")
        if (tap == 1) {
            Label1.isHidden = false
            Label1.setTextWithTypeAnimation(typedText: "Unfortunately Naples is known worldwide for it's garbage issues..  " , characterDelay: 3) //less delay is faster
            
            }
    else if (tap == 2){
        textLabel1.isHidden = false
            Label4.isHidden = true
            omiView.isHidden = true
            omi2View.isHidden = true
            omi3View.isHidden = true
            traView.isHidden = true
            tra2View.isHidden = true
            tra3View.isHidden = true
            backView.isHidden = true
             Label1.isHidden = true
            RecycleView.isHidden = false
            
           }
        
    else if (tap == 3) {
        textLabel1.isHidden = false
      Label1.isHidden = true
            Label4.isHidden = true
            textLabel1.setTextWithTypeAnimation(typedText: "       But never give up!  \n  Everyone of us can make \n the difference for our city \n        and our world! " , characterDelay: 3) //less delay is faster
    }
        
    else if (tap == 4){
            Label4.isHidden = false
        
        }
        else if (tap == 5){
            MusicPlayer.shared.stopBackgroundMusic()
            
        }
}
}
extension UILabel {
    func setTextWithTypeAnimation(typedText: String, characterDelay: TimeInterval = 5.0) {
        text = ""
        var writingTask: DispatchWorkItem?
        writingTask = DispatchWorkItem { [weak weakSelf = self] in
            for character in typedText {
                DispatchQueue.main.async {
                    weakSelf?.text!.append(character)
                }
                Thread.sleep(forTimeInterval: characterDelay/100)
            }
        }

        if let task = writingTask {
            let queue = DispatchQueue(label: "typespeed", qos: DispatchQoS.userInteractive)
            queue.asyncAfter(deadline: .now() + 0.05, execute: task)
        }
    }

}
// Present the view controller in the Live View window
PlaygroundPage.current.liveView = MyViewController()
