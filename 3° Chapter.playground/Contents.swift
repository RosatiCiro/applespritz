//: A UIKit based Playground for presenting user interface
  
import UIKit
import PlaygroundSupport
import AVFoundation




    
    
class MusicPlayer {
    static let shared = MusicPlayer()
    var audioPlayer: AVAudioPlayer?

    func startBackgroundMusic() {
        if let bundle = Bundle.main.path(forResource: "musica3", ofType: "mp3") {
            let backgroundMusic = NSURL(fileURLWithPath: bundle)
            do {
                audioPlayer = try AVAudioPlayer(contentsOf:backgroundMusic as URL)
                guard let audioPlayer = audioPlayer else { return }
                audioPlayer.numberOfLoops = -1
                audioPlayer.prepareToPlay()
                audioPlayer.play()
            } catch {
                print(error)
            }
        }
    }
    
    
    func stopBackgroundMusic() {
        guard let audioPlayer = audioPlayer else { return }
        audioPlayer.stop()
    }
}




class MyViewController : UIViewController {
    
     var manView: UIImageView!
     var entranceText : UILabel!
     var textLabel: UILabel!
     var textLabel2 : UILabel!
     var textLabel3 : UILabel!
     var textLabel4 : UILabel!
     var textLabel5: UILabel!
     var textLabel6 : UILabel!
     var textLabel7 : UILabel!
     var textLabel8 : UILabel!
     var textLabel9 : UILabel!
     var recycleText : UILabel!
     var backView: UIImageView!
     var pizzaView : UIImageView!
     var patateView : UIImageView!
     var spritzView : UIImageView!
     var cutleryView: UIImageView!
     var orangeView : UIImageView!
     var glassView : UIImageView!
     var strawView : UIImageView!
     var trashBoxView: UIImageView!
     var canView : UIImageView!
     var pranzoButton : UIButton!
     var cenaButton : UIButton!
     var plasticButton : UIButton!
     var metalButton : UIButton!
     var wasteButton : UIButton!
     var orangeButton : UIButton!
     var glassButton : UIButton!
     var strawButton : UIButton!
     var trashView : UIImageView!
     var trashView1: UIImageView!
     var trashView2 : UIImageView!
     var trashView3 : UIImageView!
     var trashView4 : UIImageView!
     var trashView5 : UIImageView!
    var finalView: UIImageView!
     
     var tap = 0
    
    
    
    override func loadView() {
        let view = UIView()
        view.backgroundColor = .white
        
        
        //sfondo
        backView = UIImageView()
        backView.image = UIImage ( named: "Landscape copia.png")
        backView.frame = CGRect (x: 0, y: 80, width: 320, height: 250)
        view.addSubview(backView)
        backView.isHidden = true
        
        
        
        //ingresso
        
        entranceText = UILabel ()
        entranceText.font = UIFont (name: "Futura", size: 20)
        entranceText.textColor = .systemGreen
        entranceText.frame = CGRect (x: 25, y: 380, width: 250, height: 100)
        view.addSubview(entranceText)
        entranceText.numberOfLines = 8
        entranceText.isHidden = true
        self.view = view
        
        
        
        // Entrata Presentazione Testo 1
        
        textLabel = UILabel ()
        
        textLabel.font = UIFont (name: "Futura", size: 20)
        textLabel.textColor = .systemGreen
        textLabel.frame = CGRect(x: 25, y: 380, width: 200, height: 100)
        view.addSubview(textLabel)
        textLabel.numberOfLines = 8
        textLabel.isHidden = true
        self.view = view
        
        
        //testo aperitivo 2
        
        textLabel2 = UILabel()
        
        textLabel2.font = UIFont (name: "Futura", size: 20)
        textLabel2.textColor = .systemGreen
        textLabel2.frame = CGRect(x: 40, y: 350, width: 300, height: 300)
        view.addSubview(textLabel2)
        textLabel2.numberOfLines = 9
        self.view = view
        textLabel2.isHidden = true
        
        //testo riciclo aperitivo 3
        
        textLabel3 = UILabel ()
        
        textLabel3.font = UIFont (name: "Futura", size: 20)
        textLabel3.textColor = .systemGreen
        textLabel3.frame = CGRect(x: 60, y: 400, width: 250, height: 200)
        view.addSubview(textLabel3)
         textLabel3.numberOfLines = 8
        self.view = view
        textLabel3.isHidden = true
        
        //domanda pranzo o cena 4
        
        textLabel4 = UILabel ()
        
        textLabel4.font = UIFont (name: "Futura", size: 20)
        textLabel4.textColor = .systemGreen
        textLabel4.frame = CGRect(x: 15, y: 395, width: 220, height: 60)
        view.addSubview(textLabel4)
        textLabel4.numberOfLines = 8
        textLabel4.isHidden = true
        self.view = view
        
        //introduzione pizza 5
        
        textLabel5 = UILabel ()
        
        textLabel5.font = UIFont (name: "Futura", size: 20)
        textLabel5.textColor = .systemGreen
        textLabel5.frame = CGRect(x: 16, y: 460, width: 195, height: 120)
        view.addSubview(textLabel5)
        textLabel5.numberOfLines = 8
        textLabel5.isHidden = true
        
        self.view = view
        
        //introduzione pasta 6
        
        textLabel6 = UILabel ()
        
        textLabel6.font = UIFont (name: "Futura", size: 20)
        textLabel6.textColor = .systemGreen
        textLabel6.frame = CGRect(x: 13, y: 460, width: 250, height: 100)
        view.addSubview(textLabel6)
        textLabel6.numberOfLines = 8
        textLabel6.isHidden = true
        self.view = view

        
        //domanda se è piaciuto 7
        
        textLabel7 = UILabel ()
        textLabel7.font = UIFont (name: "Futura", size: 20)
        textLabel7.textColor = .systemGreen
        textLabel7.frame = CGRect(x: 10, y: 460, width: 220, height: 100)
        view.addSubview(textLabel7)
        textLabel7.numberOfLines = 8
        textLabel7.isHidden = true
        self.view = view
        
        //messaggio di riciclo
        
        recycleText = UILabel()
        recycleText.font = UIFont (name: "Futura", size: 20)
        recycleText.textColor = .systemGreen
        recycleText.frame = CGRect(x: 13, y: 460, width: 240, height: 70)
        view.addSubview(recycleText)
        recycleText.numberOfLines = 8
        recycleText.isHidden = true
        
        //intestazione pagina riciclo 8
        
        textLabel8 = UILabel ()
        textLabel8.text = "Recycle with me"
        textLabel8.font = UIFont ( name : "Futura" , size: 50)
        textLabel8.textColor = .black
        textLabel8.frame = CGRect(x: 10, y: 20, width: 570, height: 70)
        view.addSubview(textLabel8)
        self.view = view
        textLabel8.isHidden = true
        
        //to be continued finale 9
        
        textLabel9 = UILabel()
        textLabel9.text = "TO BE CONTINUED..."
        textLabel9.font = UIFont (name: "Futura", size: 25)
        textLabel9.textColor = .systemGreen
        textLabel9.frame = CGRect(x: 100 , y: 500, width: 400, height: 300)
        view.addSubview(textLabel9)
        textLabel9.isHidden = true
        self.view = view
        
        
        
//                  Fine
        finalView = UIImageView ()
        finalView.image = UIImage (named: "tobe.png")
        finalView.frame = CGRect(x:45, y:250, width: 280, height: 200)
        view.addSubview(finalView)
        finalView.isHidden = true
           
        
        
        // IMMAGINI VARIE
        
        
        
        
        //immagini spritz
        
        spritzView = UIImageView ()
        spritzView.image = UIImage (named : "Spritz.png")
        spritzView.frame = CGRect (x:110 , y:80 ,width: 150, height: 300)
        view.addSubview(spritzView)
        
        
        //immagini riciclo spritz
        
        orangeView = UIImageView ()
        orangeView.image = UIImage (named : "arancia.jpeg")
        orangeView.frame = CGRect (x:27 , y:175 ,width: 70, height: 70)
        view.addSubview(orangeView)
       
        
        glassView = UIImageView ()
        glassView.image = UIImage (named : "bicchiere.png")
        glassView.frame = CGRect (x:145 , y:150 ,width: 70, height: 120)
        view.addSubview(glassView)
        
            
        strawView = UIImageView ()
        strawView.image = UIImage (named : "straw.png")
        strawView.frame = CGRect (x:285 , y:150 ,width: 30, height: 120)
        view.addSubview(strawView)
        
        
        //cestini riciclo spritz
        
        trashView1 = UIImageView ()
        trashView1.image = UIImage (named : "glass_SCR.png")
        trashView1.frame = CGRect (x:15 , y:527 ,width: 115, height: 150)
        view.addSubview(trashView1)
        
         
         
        trashView2 = UIImageView ()
        trashView2.image = UIImage (named : "plastic_SCR.png")
        trashView2.frame = CGRect (x:130 , y:527 ,width: 115, height: 150)
        view.addSubview(trashView2)
        
         
        trashView = UIImageView ()
        trashView.image = UIImage (named : "organic_SCR.png")
        trashView.frame = CGRect (x:245 , y:527 ,width: 115, height: 150)
        view.addSubview(trashView)
        
        
        //immagine pizza
        
        pizzaView = UIImageView()
        pizzaView.image = UIImage(named: "Pizza.png")
        pizzaView.frame = CGRect (x: 25, y: 380, width: 50, height: 70)
        view.addSubview(pizzaView)
        
        
        //immagine pasta
        patateView = UIImageView()
        patateView.image = UIImage (named: "spaghetti.png")
        patateView.frame = CGRect (x: 25, y: 380, width: 50, height: 70)
        view.addSubview(patateView)
        
        //immagini riciclo pasta e pizza
        
        trashBoxView = UIImageView()
        trashBoxView.image = UIImage (named: "trash.png")
        trashBoxView.frame = CGRect (x: 30, y: 175, width: 70, height: 70)
        view.addSubview(trashBoxView)
        
        
        cutleryView = UIImageView ()
        cutleryView.image = UIImage(named: "posate.png")
        cutleryView.frame = CGRect (x: 145, y: 175, width: 70, height: 70)
        view.addSubview(cutleryView)
        
        
        canView = UIImageView ()
        canView.image = UIImage (named: "can-2.png")
        canView.frame = CGRect (x: 260, y: 175, width: 70, height: 70)
        view.addSubview(canView)
        
        //cassonetti riciclo pastra o pizza
        
        trashView3 = UIImageView()
        trashView3.image = UIImage (named: "waste_SCR.png")
        trashView3.frame = CGRect (x: 245, y: 527, width: 115, height: 150)
        view.addSubview(trashView3)
        
        trashView4  = UIImageView()
        trashView4.image = UIImage (named: "metal_SCR.png")
        trashView4.frame = CGRect (x: 130, y: 527, width: 115, height: 150)
        view.addSubview(trashView4)
        
        trashView5 = UIImageView ()
        trashView5.image = UIImage (named : "plastic_SCR.png")
        trashView5.frame = CGRect (x:15 , y:527 ,width: 115, height: 150)
        view.addSubview(trashView5)
        
      

        
        
        // inizializzazione bottoni cestini
        
        orangeButton = UIButton ()
        orangeButton.setTitle("Throw me", for:.normal)
        orangeButton.setTitleColor(.white, for: .normal)
        
        orangeButton.frame = CGRect ( x: 15, y: 280 ,width: 100, height:30)
        orangeButton.isUserInteractionEnabled = true
        orangeButton.titleLabel?.font =  UIFont(name: "Marker Felt" , size: 16)
        orangeButton.backgroundColor = .brown
        view.addSubview(orangeButton)
                     
        glassButton = UIButton ()
        glassButton.setTitle("Throw me", for:.normal)
        glassButton.setTitleColor(.white, for: .normal)
        glassButton.frame = CGRect ( x: 135, y: 280 ,width: 100, height: 30)
        glassButton.isUserInteractionEnabled = true
        glassButton.titleLabel?.font =  UIFont(name: "Marker Felt" , size: 16)
        glassButton.backgroundColor = .systemGreen
        view.addSubview(glassButton)
                          
        strawButton = UIButton ()
        strawButton.setTitle("Throw me", for:.normal)
        strawButton.setTitleColor(.white, for: .normal)
        strawButton.frame = CGRect ( x: 260, y: 280 ,width: 100, height: 30)
        strawButton.isUserInteractionEnabled = true
        strawButton.titleLabel?.font =  UIFont(name: "Marker Felt" , size: 16)
        strawButton.backgroundColor = .systemYellow
        view.addSubview(strawButton)
        
        wasteButton = UIButton ()
        wasteButton.setTitle("Throw me", for:.normal)
        wasteButton.setTitleColor(.white, for: .normal)
        wasteButton.frame = CGRect ( x: 15, y: 280 ,width: 100, height:30)
        wasteButton.isUserInteractionEnabled = true
        wasteButton.titleLabel?.font =  UIFont(name: "Marker Felt" , size: 16)
        wasteButton.backgroundColor = .systemGray2
        view.addSubview(wasteButton)
                     
        plasticButton = UIButton ()
        plasticButton.setTitle("Throw me", for:.normal)
        plasticButton.setTitleColor(.white, for: .normal)
        plasticButton.frame = CGRect ( x: 135, y: 280 ,width: 100, height: 30)
        plasticButton.isUserInteractionEnabled = true
        plasticButton.titleLabel?.font =  UIFont(name: "Marker Felt" , size: 16)
        plasticButton.backgroundColor = .systemYellow
        view.addSubview(plasticButton)
                          
        metalButton = UIButton ()
        metalButton.setTitle("Throw me", for:.normal)
        metalButton.setTitleColor(.white, for: .normal)
        metalButton.frame = CGRect ( x: 260, y: 280 ,width: 100, height: 30)
        metalButton.isUserInteractionEnabled = true
        metalButton.titleLabel?.font =  UIFont(name: "Marker Felt" , size: 16)
        metalButton.backgroundColor = .systemBlue
        view.addSubview(metalButton)
        
        //Gennaro
        
        manView = UIImageView ()
        manView.image = UIImage (named: "gennaro.png")
        manView.frame = CGRect (x: 250, y: 380, width: 80, height: 190)
        view.addSubview(manView)
        manView.isHidden = true
                     
        
        
        
        
        
        //tap bottoni cestini
        let orange = UITapGestureRecognizer (target: self,action:
        #selector (taps))
                    
        orangeButton.addGestureRecognizer(orange)
                   
        let glass = UITapGestureRecognizer (target: self,action:
        #selector (tapp))
                          
        glassButton.addGestureRecognizer(glass)
                     
        let straw = UITapGestureRecognizer (target: self,action:
        #selector (tappp))
                            
        strawButton.addGestureRecognizer(straw)
        
        let pizzaWaste = UITapGestureRecognizer (target: self,action:
        #selector (tap1))
                 
        wasteButton.addGestureRecognizer(pizzaWaste)
               
        let plasticCutlery = UITapGestureRecognizer (target: self,action:
        #selector (tap2))
                      
        plasticButton.addGestureRecognizer(plasticCutlery)
                 
        let metalCan = UITapGestureRecognizer (target: self,action:
        #selector (tap3))
                        
        metalButton.addGestureRecognizer(metalCan)
           
        // tap schermo
        
        
        let gestureTapRecognizer = UITapGestureRecognizer ( target: self, action: #selector (buttonTapped) )
        self.view.addGestureRecognizer(gestureTapRecognizer)
        
        
        // inizializzazione bottoni scelta pranzo cena
        
               
        pranzoButton = UIButton ()
        pranzoButton.setTitle("Lunch", for: .normal)
        pranzoButton.setTitleColor(.white, for: .normal)
        pranzoButton.frame = CGRect ( x: 125, y: 500 ,width: 100, height: 30)
        pranzoButton.isUserInteractionEnabled = true
        pranzoButton.titleLabel?.font =  UIFont(name: "Marker Felt" , size: 20)
        pranzoButton.backgroundColor = .systemGreen
        view.addSubview(pranzoButton)
        
               
        cenaButton = UIButton ()
        cenaButton.setTitle("Dinner", for: .normal)
        cenaButton.setTitleColor(.white, for: .normal)
        
        cenaButton.frame = CGRect ( x: 15, y: 500 ,width: 100, height: 30)
        cenaButton.isUserInteractionEnabled = true
        cenaButton.titleLabel?.font =  UIFont(name: "Marker Felt" , size: 20)
        cenaButton.backgroundColor = .systemGreen
        view.addSubview(cenaButton)
               
        //tap bottoni cena
               
        let cena = UITapGestureRecognizer (target: self,action:
                       #selector (tapCena))
                   
        cenaButton.addGestureRecognizer(cena)
                 
        let pranzo = UITapGestureRecognizer (target: self,action:
                            #selector (tapPranzo))
                 
        pranzoButton.addGestureRecognizer(pranzo)
        
        
        
        
        
        
        
        //oggetti nascosti
        
        manView.isHidden = true
        entranceText.isHidden = true
        textLabel.isHidden = true
        textLabel2.isHidden = true
        textLabel3.isHidden = true
        textLabel4.isHidden = true
        textLabel5.isHidden = true
        textLabel6.isHidden = true
        textLabel7.isHidden = true
        textLabel8.isHidden = true
        textLabel9.isHidden = true
        recycleText.isHidden = true
        backView.isHidden = true
        pizzaView.isHidden = true
        patateView.isHidden = true
        spritzView.isHidden = true
        cutleryView.isHidden = true
        orangeView.isHidden = true
        glassView.isHidden = true
        strawView.isHidden = true
        trashBoxView.isHidden = true
        canView.isHidden = true
        pranzoButton.isHidden = true
        cenaButton.isHidden = true
        plasticButton.isHidden = true
        metalButton.isHidden = true
        wasteButton.isHidden = true
        orangeButton.isHidden = true
        glassButton.isHidden = true
        strawButton.isHidden = true
        trashView.isHidden = true
        trashView1.isHidden = true
        trashView2.isHidden = true
        trashView3.isHidden = true
        trashView4.isHidden = true
        trashView5.isHidden = true
        
        
        
        
        
        
        
       
      
        self.view = view
    }
    
    
    
    
    //inizio click schermo
    
    
    @objc func buttonTapped () {
         tap += 1
        print ("tapped")
        
        
        if (tap == 1){
        MusicPlayer.shared.startBackgroundMusic()
            backView.isHidden = false
            manView.isHidden = false
            
        }
        else if (tap == 2){
            entranceText.isHidden = false
            entranceText.setTextWithTypeAnimation(typedText:  "Now come with me!..." , characterDelay: 3) //less delay is faster
            
        }
        
        else if ( tap == 3){
    
            entranceText.isHidden = true
            textLabel.isHidden = false
            textLabel.setTextWithTypeAnimation(typedText:  " But before, let’s have an aperitivo! 🍹" , characterDelay: 3) //less delay is faster
           
    }
        else if ( tap == 4){
            backView.isHidden = true
            manView.isHidden = true
            textLabel.isHidden = true
            textLabel2.isHidden = false
            spritzView.isHidden = false
            textLabel2.setTextWithTypeAnimation(typedText:  "      Have you ever tried Spritz?\n It’s a nice orange cocktail,it isn’t too sweet or too bitter, neither too alcoholic.\nIt is made with Aperol, Prosecco and soda. Let’s go to take one! " , characterDelay: 3) //less delay is faster
            
        }
        
        else if ( tap == 5){
            textLabel2.isHidden = true
            textLabel3.isHidden = false
            textLabel3.setTextWithTypeAnimation(typedText:  "      Now that you have enjoyed your  Spritz, pay attention to how to recycle it!  There are three “materials” in  your empty spritz glass, how can you recycle it  properly?" , characterDelay: 3) //less delay is faster
        }
        
        else if ( tap == 6){
            
            textLabel3.isHidden = true
            spritzView.isHidden = true
            textLabel8.isHidden = false
            orangeView.isHidden = false
            glassView.isHidden = false
            strawView.isHidden = false
            orangeButton.isHidden = false
            glassButton.isHidden = false
            strawButton.isHidden = false
            trashView.isHidden = false
            trashView1.isHidden = false
            trashView2.isHidden = false
            
        }
        else if ( tap == 7) {
            textLabel8.isHidden = true
            orangeView.isHidden = true
            glassView.isHidden = true
            strawView.isHidden = true
            orangeButton.isHidden = true
            glassButton.isHidden = true
            strawButton.isHidden = true
            trashView.isHidden = true
            trashView1.isHidden = true
            trashView2.isHidden = true
            
            backView.isHidden = false
            manView.isHidden = false
            textLabel4.isHidden = false
             textLabel4.setTextWithTypeAnimation(typedText:  "What do you want to do?      " , characterDelay: 3) //less delay is faster
            
            
        }
        
        else if ( tap == 8){
            pranzoButton.isHidden = false
            cenaButton.isHidden = false
            
            
        }
        
        else if ( tap == 9){
            textLabel5.isHidden = true
            textLabel6.isHidden = true
            pizzaView.isHidden = true
            patateView.isHidden = true
            textLabel7.isHidden = false
             textLabel7.setTextWithTypeAnimation(typedText:  "  It was delicious, wasn't it?       " , characterDelay: 3) //less delay is faster
            
        }
        
        else if ( tap == 10){
            
            textLabel7.isHidden = true
            recycleText.isHidden = false
            recycleText.setTextWithTypeAnimation(typedText:  " Now it's time to recycle!" , characterDelay: 3) //less delay is faster
        }
        
        else if ( tap == 11) {
            recycleText.isHidden = true
            backView.isHidden = true
            manView.isHidden = true
            textLabel8.isHidden = false
            trashBoxView.isHidden = false
            cutleryView.isHidden = false
            canView.isHidden = false
            trashView5.isHidden = false
            trashView3.isHidden = false
            trashView4.isHidden = false
            metalButton.isHidden = false
            plasticButton.isHidden = false
            wasteButton.isHidden = false
            
            
            
        }
    
        else if (tap == 12){
            textLabel8.isHidden = true
            trashBoxView.isHidden = true
            cutleryView.isHidden = true
            canView.isHidden = true
            trashView5.isHidden = true
            trashView3.isHidden = true
            trashView4.isHidden = true
            metalButton.isHidden = true
            plasticButton.isHidden = true
            wasteButton.isHidden = true
            finalView.isHidden = false
            textLabel9.isHidden = false
        
        
    }
        else if ( tap == 13){
            MusicPlayer.shared.stopBackgroundMusic()
            
        }
        
    }
    
    
    
    @objc func tapCena () {
        
        textLabel4.isHidden = true
        pranzoButton.isHidden = true
        cenaButton.isHidden = true
        textLabel5.isHidden = false
          textLabel5.setTextWithTypeAnimation(typedText:  " Let's go eat the most iconic dish of Naples, LA PIZZA A PORTAFOGLIO! " , characterDelay: 3) //less delay is faster
        pizzaView.isHidden = false
        
        

        
    }
    
    @objc func tapPranzo () {
        
        textLabel4.isHidden = true
        pranzoButton.isHidden = true
        cenaButton.isHidden = true
        textLabel6.isHidden = false
         textLabel6.setTextWithTypeAnimation(typedText:  "Let's go to eat an iconic dish of Naples: PASTA PATATE E PROVOLA!" , characterDelay: 3) //less delay is faster
        patateView.isHidden = false
        
        

    }
    
    @objc func taps () {
        
        UIView.animate(withDuration: 3.0) {
        self.orangeView.transform = CGAffineTransform(translationX:245, y:400)}
    orangeButton.isHidden = true
            }
    @objc    func tapp () {
            UIView.animate(withDuration: 3.0) {
                self.glassView.transform = CGAffineTransform(translationX:-110, y:400)
               
        }
        glassButton.isHidden = true
    }
       
      @objc    func tappp () {
        UIView.animate(withDuration: 3.0) {self.strawView.transform = CGAffineTransform(translationX:-110, y:400)}
        strawButton.isHidden = true
    }
    
    
    @objc func tap1 () {
        
        UIView.animate(withDuration: 3.0) {
        self.trashBoxView.transform = CGAffineTransform(translationX:245, y:400)}
    wasteButton.isHidden = true
            }
    @objc    func tap2 () {
            UIView.animate(withDuration: 3.0) {
                self.cutleryView.transform = CGAffineTransform(translationX:-110, y:400)
               
        }
        plasticButton.isHidden = true
    }
       
      @objc    func tap3 () {
        UIView.animate(withDuration: 3.0) {self.canView.transform = CGAffineTransform(translationX:-110, y:400)}
        metalButton.isHidden = true
    }
}
extension UILabel {
    func setTextWithTypeAnimation(typedText: String, characterDelay: TimeInterval = 5.0) {
        text = ""
        var writingTask: DispatchWorkItem?
        writingTask = DispatchWorkItem { [weak weakSelf = self] in
            for character in typedText {
                DispatchQueue.main.async {
                    weakSelf?.text!.append(character)
                }
                Thread.sleep(forTimeInterval: characterDelay/100)
            }
        }

        if let task = writingTask {
            let queue = DispatchQueue(label: "typespeed", qos: DispatchQoS.userInteractive)
            queue.asyncAfter(deadline: .now() + 0.05, execute: task)
        }
    }

}
// Present the view controller in the Live View window
PlaygroundPage.current.liveView = MyViewController()
