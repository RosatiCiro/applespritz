//: A UIKit based Playground for presenting user interface
  
import UIKit
import PlaygroundSupport
import AVFoundation

class MusicPlayer {
    static let shared = MusicPlayer()
    var audioPlayer: AVAudioPlayer?

    func startBackgroundMusic() {
        if let bundle = Bundle.main.path(forResource: "musica4", ofType: "mp3") {
            let backgroundMusic = NSURL(fileURLWithPath: bundle)
            do {
                audioPlayer = try AVAudioPlayer(contentsOf:backgroundMusic as URL)
                guard let audioPlayer = audioPlayer else { return }
                audioPlayer.numberOfLoops = -1
                audioPlayer.prepareToPlay()
                audioPlayer.play()
            } catch {
                print(error)
            }
        }
    }
    
    
    func stopBackgroundMusic() {
        guard let audioPlayer = audioPlayer else { return }
        audioPlayer.stop()
    }
}





class MyViewController : UIViewController {
     var  trash1View : UIImageView!
    var  rainView : UIImageView!
      var  sunView : UIImageView!
   var  trashSmall1View : UIImageView!
   var  trashSmall2View : UIImageView!
   var  trashSmall3View : UIImageView!
    var  trashView : UIImageView!
    var  recycleView : UIImageView!
     var  cuoreView : UIImageView!
    var  black3View : UIImageView!
    var  black1View : UIImageView!
    var  black2View : UIImageView!
    var  trashGView : UIImageView!
    var  trashMView : UIImageView!
    var  trashOView : UIImageView!
    var  trashPView : UIImageView!
    var finalView : UIImageView!
    
    var landScapeView : UIImageView!
    var manView : UIImageView!
    var manHappyView : UIImageView!
     var man1View : UIImageView!
    var man2View : UIImageView!
    var streetView : UIImageView!
    var Label1 : UILabel!
    var tap = 0
    var ominofelice1View : UIImageView!
    var ominofelice2View : UIImageView!
    var ominofelice3View : UIImageView!
    var textLabel4 : UITextView!
    
    
    override func loadView() {
        let view = UIView()
        view.backgroundColor = .white
        self.view = view
        MusicPlayer.shared.startBackgroundMusic()
//        Prima scritta
        Label1 = UILabel()
        
        Label1.font = UIFont (name: "Futura", size: 20)
        Label1.textColor = .systemGreen
        Label1.frame = CGRect(x: 10, y: 280, width: 250, height: 350)
        Label1.numberOfLines = 8
        view.addSubview(Label1)
        
        // scritta finale
        
        textLabel4 = UITextView ()
        textLabel4.text = "THE END"
        textLabel4.font = UIFont (name: "Futura", size: 40)
        textLabel4.textColor = .systemGreen
        textLabel4.frame = CGRect(x: 170 , y: 470, width: 400, height: 380)
        view.addSubview(textLabel4)
        textLabel4.isHidden = true
        self.view = view
        
        //        Strada
        streetView = UIImageView()
       streetView.image = UIImage (named: "street.png")
                     streetView.frame = CGRect (x: 50, y:370, width:200, height: 200)
                      view.addSubview(streetView)
//        Vesuvio
       landScapeView = UIImageView()
       landScapeView.image = UIImage (named: "Vesuvio copia.png")
                     landScapeView.frame = CGRect (x: 50, y:150, width:320, height: 150)
                     view.addSubview(landScapeView)
        //        Nuvola
       rainView = UIImageView()
        rainView.image = UIImage (named: "cloud 2.png")
        rainView.frame = CGRect (x: 40, y:50, width:100, height: 100)
                      view.addSubview(rainView)
        //        sole
             sunView = UIImageView()
              sunView.image = UIImage (named: "sun.png")
             sunView.frame = CGRect (x: 40, y:50, width:100, height: 100)
                            view.addSubview(sunView)
//        Gennaro triste
        manView = UIImageView ()
         manView.image = UIImage(named: "Genny triste.png")
                      manView.frame = CGRect(x: 250, y: 400, width: 80, height: 200)
                      view.addSubview(manView)
        //        Gennaro felice grande
        manHappyView = UIImageView ()
         manHappyView.image = UIImage(named: "Gennaro indica.png")
                      manHappyView.frame = CGRect(x: 250, y: 400, width: 80, height: 200)
                      view.addSubview(manHappyView)
        //        omino faccia nera 1
        black1View = UIImageView ()
          black1View.image = UIImage(named: "Amica2.png")
                     black1View.frame = CGRect(x: 20, y: 450, width: 40, height: 100)
                      view.addSubview(black1View)
        //        omino faccia nera 2
         black2View = UIImageView ()
         black2View.image = UIImage(named: "Amico1.png")
                     black2View.frame = CGRect(x: 60, y: 350, width: 40, height: 100)
                      view.addSubview(black2View)
        //         omino faccia nera 3
         black3View = UIImageView ()
         black3View.image = UIImage(named: "Amico3.png")
         black3View.frame = CGRect(x: 110, y: 500, width: 40, height: 100)
        
        //finalView
        
        finalView = UIImageView ()
        finalView.image = UIImage (named: "theend.png")
        finalView.frame = CGRect(x:45, y:75, width: 300, height: 330)
        view.addSubview(finalView)
        finalView.isHidden = true
        
                      view.addSubview(black3View)
        //        Gennaro felice 3
           man2View = UIImageView ()
           man2View.image = UIImage(named: "Gennaro indica.png")
                        man2View.frame = CGRect(x: 300, y: 490, width: 65, height: 180)
                        view.addSubview(man2View)
        //        omino felice 1
                  ominofelice1View = UIImageView ()
                  ominofelice1View.image = UIImage(named: "Amica2felice.png")
                               ominofelice1View.frame = CGRect(x: 20, y: 450, width: 40, height: 100)
                               view.addSubview(ominofelice1View)
        //        omino felice 2
                         ominofelice2View = UIImageView ()
                         ominofelice2View.image = UIImage(named: "Amico1felice.png")
                                      ominofelice2View.frame = CGRect(x: 60, y: 350, width: 40, height: 100)
                                      view.addSubview(ominofelice2View)
        //        omino felice 3
                         ominofelice3View = UIImageView ()
                         ominofelice3View.image = UIImage(named: "Amico3felice.png")
                                      ominofelice3View.frame = CGRect(x: 110, y: 500, width: 40, height: 100)
                                      view.addSubview(ominofelice3View)
        //        cartaccia 1
               trashSmall1View = UIImageView ()
                 trashSmall1View.image = UIImage(named: "trash small.png")
                            trashSmall1View.frame = CGRect(x: 60, y: 550, width: 20, height: 20)
                             view.addSubview(trashSmall1View)
        //        cartaccia 2
                      trashSmall2View = UIImageView ()
                        trashSmall2View.image = UIImage(named: "trash small.png")
                                   trashSmall2View.frame = CGRect(x: 110, y: 450, width: 20, height: 20)
                                    view.addSubview(trashSmall2View)
        //        cartaccia 3
                      trashSmall3View = UIImageView ()
                        trashSmall3View.image = UIImage(named: "trash small.png")
                                   trashSmall3View.frame = CGRect(x: 150, y: 600, width: 20, height: 20)
                                    view.addSubview(trashSmall3View)
              
                           
        
      
        //        spazzatura1
        trashView = UIImageView ()
        trashView.image = UIImage(named: "trash.png")
                     trashView.frame = CGRect(x:120, y: 330, width: 60, height: 80)
                     view.addSubview(trashView)

        //        spazzatura2
              trash1View = UIImageView ()
              trash1View.image = UIImage(named: "trash.png")
                           trash1View.frame = CGRect(x:180, y: 330, width: 60, height: 80)
                           view.addSubview(trash1View)
        //        spazzatura vetro
                     trashGView = UIImageView ()
                     trashGView.image = UIImage(named: "trash glass.png")
                                  trashGView.frame = CGRect(x:120, y: 320, width: 40, height: 65)
                                  view.addSubview(trashGView)
        //        spazzatura metal
        trashMView = UIImageView ()
        trashMView.image = UIImage(named: "trash metal.png")
                     trashMView.frame = CGRect(x:160, y: 320, width: 40, height: 65)
                     view.addSubview(trashMView)
        //        spazzatura organic
        trashOView = UIImageView ()
        trashOView.image = UIImage(named: "trash organic.png")
                     trashOView.frame = CGRect(x:200, y: 320, width: 40, height: 65)
                     view.addSubview(trashOView)
        //        spazzatura plastic
        trashPView = UIImageView ()
        trashPView.image = UIImage(named: "trash plastic.png")
                     trashPView.frame = CGRect(x:240, y: 320, width: 40, height: 65)
                     view.addSubview(trashPView)
        //        Gennaro triste
              man1View = UIImageView ()
              man1View.image = UIImage(named: "Genny triste.png")
                           man1View.frame = CGRect(x: 300, y: 450, width: 65, height: 180)
                           view.addSubview(man1View)
        //        cuore
               cuoreView = UIImageView ()
               cuoreView.image = UIImage(named: "cuore.png")
                            cuoreView.frame = CGRect(x:140, y: 20, width: 80, height: 80)
                            view.addSubview(cuoreView)
        //        recycle
                     recycleView = UIImageView ()
                     recycleView.image = UIImage(named: "Recycle.png")
                                  recycleView.frame = CGRect(x:140, y: 200, width: 80, height: 80)
                                  view.addSubview(recycleView)
        
        let gestureTapRecognizer = UITapGestureRecognizer ( target: self, action: #selector(buttonTapped) )
            
        self.view.addGestureRecognizer(gestureTapRecognizer)
   
//    isHidden
        streetView.isHidden = true
         trashMView.isHidden = true
        trashGView.isHidden = true
        trashPView.isHidden = true
        trashOView.isHidden = true
        man1View.isHidden = true
        man2View.isHidden = true
        sunView.isHidden = true
        ominofelice1View.isHidden = true
         ominofelice2View.isHidden = true
         ominofelice3View.isHidden = true
        manHappyView.isHidden = true
        recycleView.isHidden = true
        cuoreView.isHidden = true
        textLabel4.isHidden = true
        Label1.isHidden = true
        
    }
    
      @objc func buttonTapped (){
                  tap += 1
                  print ("tapped")
                  if (tap == 1){
                    trashSmall2View.isHidden = true
                    black2View.isHidden = true
                          ominofelice2View.isHidden = false
                          
                  }
                  else if (tap == 2) {
                    black1View.isHidden = true
                      ominofelice1View.isHidden = false
                    trashSmall1View.isHidden = true
                  
                   
        }
        else if (tap == 3) {
                    black3View.isHidden = true
                      ominofelice3View.isHidden = false
                    trashSmall3View.isHidden = true
                           
               }
        else if (tap == 4) {
                      manHappyView.isHidden = false
                    manView.isHidden = true
                    rainView.isHidden = true
                    sunView.isHidden = false
                    trashView.isHidden = true
                    trash1View.isHidden = true
                               trashMView.isHidden = false
                               trashGView.isHidden = false
                    trashOView.isHidden = false
                    trashPView.isHidden = false
                    }
        else if (tap == 5) {
                    manHappyView.isHidden = false
                                      manView.isHidden = true
                                      rainView.isHidden = true
                                      sunView.isHidden = false
                                      trashView.isHidden = true
                                      trash1View.isHidden = true
                                                 trashMView.isHidden = true
                                                 trashGView.isHidden = true
                                      trashOView.isHidden = true
                                      trashPView.isHidden = true
                                ominofelice3View.isHidden = true
                    ominofelice1View.isHidden = true
                     ominofelice2View.isHidden = true
                    }
        else if (tap == 6) {
                  Label1.isHidden = false
                    Label1.setTextWithTypeAnimation(typedText:  "It has been a wonderful\n day! Thank you for \nhelping me! And remember, recycling is important for your planet, your city and your life!" , characterDelay: 3) //less delay is faster
                                   
                         }
        else if (tap == 7) {
                 ominofelice3View.isHidden = true
                 ominofelice1View.isHidden = true
                 ominofelice2View.isHidden = true
                 recycleView.isHidden = true
                 cuoreView.isHidden = true
                 manHappyView.isHidden = true
                 textLabel4.isHidden = false
                 finalView.isHidden = false
                 landScapeView.isHidden = true
                 sunView.isHidden = true
                 Label1.isHidden = true
        }
        
                  else if ( tap == 8 ){
                    MusicPlayer.shared.stopBackgroundMusic()
        }
        

    }
}
extension UILabel {
    func setTextWithTypeAnimation(typedText: String, characterDelay: TimeInterval = 5.0) {
        text = ""
        var writingTask: DispatchWorkItem?
        writingTask = DispatchWorkItem { [weak weakSelf = self] in
            for character in typedText {
                DispatchQueue.main.async {
                    weakSelf?.text!.append(character)
                }
                Thread.sleep(forTimeInterval: characterDelay/100)
            }
        }

        if let task = writingTask {
            let queue = DispatchQueue(label: "typespeed", qos: DispatchQoS.userInteractive)
            queue.asyncAfter(deadline: .now() + 0.05, execute: task)
        }
    }

}
// Present the view controller in the Live View window
PlaygroundPage.current.liveView = MyViewController()
